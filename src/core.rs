pub mod core{
use std::io;

    pub fn process_score(score: (f64,u32)){
        let mut name = String::new();
        
        println!("Please input your name");
        io::stdin().read_line(&mut name).expect("Failed to read line");
        let name = name.trim();

        let score: (&str,f64) = (&name,score.0);

        let mut scores = get_scores();

        scores.push(score);
        //TODO sort

        print_highest_scores(&scores);

        //TODO save_scores(&scores);
    }
    
    fn get_scores() -> Vec<(&'static str,f64)>{
        let mut scores = Vec::new();
        scores.push(("Andre",0.001));
        scores.push(("Sofia",0.002));
        scores.push(("Vera" ,0.01));
        scores
    }

    fn _save_scores(_scores: & Vec<(&'static str, f64)>){
        //TODO do nothing
    }

    fn print_highest_scores(high_scores: &[(&str,f64)]) {
        let max_scores = if high_scores.len() < 10 { high_scores.len()} else { 10 };
        let high_scores = &high_scores[..max_scores];
        println!("Printing the highest scores:");
        for score in high_scores.iter() {
            println!("Name: {}, Score: {}", score.0, score.1);
        }
    }
}
