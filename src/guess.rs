pub mod guess{

use std::io;
use std::cmp::Ordering;
use rand::Rng;

const MAX_GUESS : u32 = 101;

    pub fn play_game() -> (f64,u32) {
        println!("Guess the number!");

        let secret_number = rand::thread_rng().gen_range(1, MAX_GUESS);

    //    println!("The secret number is: {}", secret_number);
        let mut atempts : u32 = 1;
        let mut score : f64 = 1.0;
        loop{

            println!("Please input your guess.");

            let mut guess = String::new();

            io::stdin().read_line(&mut guess).expect("Failed to read line");

            let guess :u32 = match guess.trim().parse(){
                Ok(num) => num,
                Err(_) => continue,
            };

            println!("You guessed: {}", guess);

            match guess.cmp(&secret_number){
                Ordering::Less => {
                    let guess = process_guess("Too small",secret_number, guess);
                    score *= guess;
                }
                Ordering::Greater => {
                    let guess = process_guess("Too big",guess,secret_number);
                    score *=guess;
                }
                Ordering::Equal => {
                    println!("You win!");
                    break;
                }
            }
            atempts +=1;
        }
        (score,atempts)
    }

    fn process_guess(message: &str, guess: u32, secret: u32) -> f64{
        println!("{}", message);
        let guess : f64 = 1.0 / (guess - secret) as f64;
        guess
    }
}
